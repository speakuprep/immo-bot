var cheerio = require('cheerio');
var request = require('request');
var immoOfTheDay = [];

module.exports = function () {

  return new Promise(function (resolve, reject) {
    request({
      method: 'GET',
      url: 'https://www.ebay-kleinanzeigen.de/s-wohnung-mieten/berlin/c203l3331+wohnung_mieten.zimmer_i:4,'
      }, function(err, response, body, callback) {
        if (err) return console.error(err);

        // get the HTML body from WordThink.com
        $ = cheerio.load(body);

        if(immoOfTheDay.length > 0){
          immoOfTheDay = [];
        }

        var articles = $('.aditem');

        articles.each(function (index, item) {
          var $item = $(item);
          var img = $item.find('.imagebox').attr('data-imgsrc');
          var title = $item.find('.text-module-begin > a').text();
          var text = $item.find('.text-module-begin').next('p').text();
          var price = $item.find('.aditem-details > strong').text();
          var place = $item.find('.aditem-details');
          place.find('strong').remove();

          immoOfTheDay.push({
            'id' : index,
            'image' : img,
            'title' : title,
            'text' : text,
            'price' : price,
            'place' : place.text()
          })
        })

    });

    resolve(immoOfTheDay);    
  });

}
