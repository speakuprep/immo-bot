var express = require('express');
var crypto = require('crypto');
var requestImmos = require('./reqImmo.js');
var bot = require('./bot.js');
var app = express();
var hash;

//API to get results on request
app.get('/', function (req, res) {
  // allow access from other domains
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With');

  requestImmos().then(function (result) {
    res.send(JSON.stringify(result, null, 4));
  });


});

// start app on localhost port 3004
var port = process.env.PORT || 3004;
var server = app.listen(port, function () {
  var addr = server.address().address;
  var port = server.address().port;

  console.log('Server startet at ' + addr + ':' + port);
});


//webiste polling
setInterval(function () {
  console.log('poll');

  requestImmos().then(function (result) {
    var newHash = crypto.createHash('md5').update(result[0].title).digest('hex');
    console.log(newHash, hash);
    if (newHash !== hash) {
      hash = newHash;

      //bot post
      console.log('bot post');
      bot(result[0].title)
    }

  });

}, 30000)
